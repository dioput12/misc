alias StartupTime="[ ! -f /tmp/vim-plugins-profile.py ] && curl -Ls https://github.com/hyiltiz/vim-plugins-profile/raw/master/vim-plugins-profile.py -o /tmp/vim-plugins-profile.py; python3 /tmp/vim-plugins-profile.py nvim && python3 /tmp/vim-plugins-profile.py vim"
alias doom="~/.emacs.d/bin/doom"
alias emacsq="emacsclient -e \"(kill-emacs)"\"
alias grabber="[ -f /tmp/$(whoami)-screen-urlview ] && vi /tmp/$(whoami)-screen-urlview"
alias localhost="ssh -R 80:localhost:8080 nokey@localhost.run -F none"
alias nnn="nnn -G"
alias nnn-plugin="rm -r ~/.config/nnn/plugins* && curl -fsSL https://raw.githubusercontent.com/jarun/nnn/master/plugins/getplugs | sh"
alias root="doas su -"
alias sudoe="doas env"
alias yta="yt-dlp --extract-audio --add-metadata --xattrs --embed-thumbnail --audio-quality 0 --audio-format mp3"
alias ytv="yt-dlp --merge-output-format mp4 -f bestvideo+bestaudio[ext=m4a]/best --embed-thumbnail --add-metadata"

alias pip='function _pip(){
	if [ $1 = "search" ]; then
		pip_search "$2";
	else pip "$@";
	fi;
};_pip'

case "$(uname -s)" in
	FreeBSD)
		alias ls="ls -G"
	;;
	Linux)
		alias ls="ls --color"
	;;
	OpenBSD)
		command -v colorls >/dev/null && alias ls="colorls -G"
	;;
esac

if [ -n "$DISPLAY" ]; then
	alias emacs="emacsclient -c -n -a \"\""
else
	alias emacs="emacsclient -a \"\" --nw"
fi

function gh-get() {
	if [[ -n "$1" ]]; then
		file=${*/github.com/raw.githubusercontent.com}
		file=${file/blob/}
		wget "$file"
	else
		echo "gh-get <github link to file>"
	fi
}

function gl-get() {
	if [[ -n "$1" ]]; then
		file=${*/blob/raw}
		wget "$file"
	else
		echo "gl-get <gitlab link to file>"
	fi
}

function git-dwn() {
	if [[ -n "$1" && -n "$2" ]]; then
		repo=$(echo "$1" | sed 's/\/$\|.git$//')
		svn export "$repo/trunk/$2"
	else
		echo "git-dwn <repository> <subdirectory>"
	fi
}

function git-rebase() {
while :; do
	[ -d ./.git ] || return 1
	echo "Running git-rebase, please choose your branch."
	read -r $NFLAG -ep "(M)aster or something? "
		case $REPLY in
			M|m|'')
				git checkout master
				git pull
				git checkout -
				git rebase master "$(git rev-parse --abbrev-ref HEAD)"
				break
			;;
			*)
				git checkout $REPLY
				git pull
				git checkout -
				git rebase $REPLY "$(git rev-parse --abbrev-ref HEAD)"
				break
			;;
		esac
done
}

function share() {
	local file=${1:-/dev/stdin}
	curl --data-binary @${file} https://paste.rs && printf '\n'
}

function yank() {
	if [[ -n "$1" ]]; then
		xsel -b -i < "$1"
	else
		echo "yank foobar; yank foobaz"
	fi
}

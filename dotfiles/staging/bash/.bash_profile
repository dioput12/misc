[ -z "$(echo $BASH)" ] || PS1='\[\033[01;34m\]\w\[\033[00m\] ${GITSTATUS_PROMPT}\n \$ ' && PROMPT_DIRTRIM=3

[ -n "$(pgrep emacs)" ] || screen -dm sh -c 'emacs --fg-daemon'

if command -v tmux >/dev/null && [ -n "$PS1" ] && [ -z "$TMUX" ] && [ -z "$STY" ] && [ ! "$USER" = "root" ] ; then
	{ tmux attach || tmux; screen -xRR $(uname -m); } >/dev/null 2>&1
fi

alias StartupTime="wget -qO - https://github.com/hyiltiz/vim-plugins-profile/raw/master/vim-plugins-profile.py | python3"
alias emacs="emacsclient -a \"\" --nw -s $PREFIX/var/run/emacs$(id -u)/server"
alias emacsq="emacsclient -e \"(kill-emacs)\" -s $PREFIX/var/run/emacs$(id -u)/server"
alias grabber="[ ! -f $TMPDIR/screen-urlview ] && echo \"$TMPDIR/screen-urlview\" | termux-clipboard-set || vis $TMPDIR/screen-urlview"
alias nnn-plugin="rm -r ~/.config/nnn/plugins* && curl -fsSL https://raw.githubusercontent.com/jarun/nnn/master/plugins/getplugs | sh"
alias nnn-update="[ ! -d ~/downloads/nnn ] && git clone --depth=1 https://github.com/jarun/nnn ~/downloads/nnn; cd ~/downloads/nnn; rm -f src/nnn.c.orig && git restore src/nnn.c && git pull origin master; cat patches/gitstatus/mainline.diff | patch -p1 && make O_NERD=1; strip --strip-unneeded nnn && termux-elf-cleaner nnn && mv nnn $PREFIX/bin; cd - 1>/dev/null"
alias root="tsu"
alias vi="vis"
alias yta="yt-dlp --extract-audio --add-metadata --embed-thumbnail --audio-quality 0 --audio-format mp3"
alias ytv="yt-dlp --merge-output-format mp4 -f bestvideo+bestaudio[ext=m4a]/best --embed-thumbnail --add-metadata"

alias pip='function _pip(){
	if [ $1 = "search" ]; then
		pip_search "$2";
	else pip "$@";
	fi;
};_pip'

function share() {
	local file=${1:-/dev/fd/0}
	curl --data-binary @${file} https://paste.rs && printf '\n'
}

function yank() {
	if [[ -n "$1" ]]; then
		termux-clipboard-set < "$1"
	else
		echo "yank foobar; yank foobaz"
	fi
}

#!/usr/bin/env bash

message_format() {
	echo -e "$@" | tr -s ' '
}

unknown_response() {
	message_format "Unknown response: \"$REPLY\"."
}

case "$(hostname)" in
	localhost)
		[ ! -d $PREFIX/etc/apt ] && echo "Aborted, please change your hostname and try again later!" && exit 1 || [ -d $PREFIX/etc/apt ] && echo "You're running inside Termux environment."
		yes | pkg upgrade && termux-change-repo
		apt install -y libqrencode neovim proot-distro pass-otp python-pip stow termux-api termux-elf-cleaner termux-services vis
		ln -sf $PREFIX/bin/nvim $PREFIX/bin/vim
		cp $(pwd)/../bin/vimdiff $PREFIX/bin
		pip install pynvim
		mkdir -p ~/bin
		cp $(pwd)/../bin/termux-file-editor ~/bin
		cp $(pwd)/../bin/termux-url-opener ~/bin
		cp $(pwd)/../bin/urlview-termux $PREFIX/bin/urlview
		ln -sf $(pwd)/main/ksh/.profile.orig ~/.profile
		cp $(pwd)/staging/bash/.bash_profile ~
		mkdir -p ~/.shortcuts && cp $(pwd)/staging/termux/.shortcuts/* ~/.shortcuts
		ln -sf $PREFIX/bin/termux-clipboard-get $PREFIX/bin/p
		ln -sf $PREFIX/bin/termux-clipboard-set $PREFIX/bin/y
		ln -sf $PREFIX/bin/termux-share $PREFIX/bin/i
		rm -r $SVDIR/*
	;;
	*)
		echo "Don't forget to uninstall sudo and add your username in wheel group!" && su root -c 'curl -Ls https://github.com/jirutka/doas-sudo-shim/raw/master/sudo -o /usr/local/sbin/sudo && chmod +x /usr/local/sbin/sudo; cat $(pwd)/../etc/doas.conf > /etc/doas.conf'
	;;
esac

while :; do
	echo "Install asdf and etcetera?"
	read -r $NFLAG -ep "(Y)es or (n)o: "
		case $REPLY in
			Y|y|'')
				# nnn
				echo "Please grab latest version of nnn!"
				xdg-open https://github.com/jarun/nnn/tags

				# font
				echo "Will install FiraCode font, after that you can remove it later."
				mkdir -p ~/.local/share/fonts && cp -v $(pwd)/main/termux/.termux/font.ttf ~/.local/share/fonts/firacode-nerd.ttf

				# asdf & bash
				git clone --depth=1 https://github.com/asdf-vm/asdf ~/.asdf
				echo "Please recheck your $HOME/.bash_local file!"
				cp -v $(pwd)/staging/bash/.bash_local ~/.bash_local
				break
			;;
			N|n)
				echo "Aborted, please try again later."
				exit 1
			;;
			*)
				unknown_response
			;;
		esac
done

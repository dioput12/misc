;;; init.el -*- lexical-binding: t; -*-

(doom! :completion
       (company +childframe)
       (vertico +icons)

       :ui
       doom
       doom-dashboard
       hl-todo
       (modeline +light)
       neotree
       ophints
       (popup +defaults)
       vc-gutter
       vi-tilde-fringe
       workspaces
       zen

       :editor
       (evil +everywhere)
       file-templates
       fold
       format
       snippets
       word-wrap

       :emacs
       (dired +icons)
       electric
       undo
       vc

       :term
       eshell
       vterm

       :checkers
       syntax
       (spell +flyspell)
       grammar

       :tools
       (eval +overlay)
       lookup
       lsp
       magit
       tmux

       :os
       (:if IS-MAC macos)
       tty

       :lang
       (cc +lsp)
       emacs-lisp
       markdown
       org
       (python +cython +lsp +pyright +pyenv)
       sh

       :config
       ;;literate
       (default +bindings +smartparens))

;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-
;; General
(setq user-full-name "Dio Putra"
      user-mail-address "dioput12@gmail.com")

(setq doom-theme 'doom-gruvbox)

(setq doom-font (font-spec :family "monospace" :size 16 :weight 'semi-light)
      doom-variable-pitch-font (font-spec :family "sans" :size 13))

(add-to-list 'default-frame-alist '(inhibit-double-buffering . t))
(set-frame-parameter (selected-frame) 'fullscreen 'maximized)
(add-to-list 'default-frame-alist '(fullscreen . maximized))

'(mouse-wheel-progressive-speed nil)
'(mouse-wheel-scroll-amount '(1 ((shift) . 5) ((control))))

(setq evil-split-window-below t
      evil-vsplit-window-right t)

(setq org-directory "~/doc/org/")

;; Mixed
(when (fboundp 'scroll-bar-mode)
      (menu-bar-mode 1)
      (scroll-bar-mode 1))

(remove-hook! '+doom-dashboard-functions #'doom-dashboard-widget-banner #'doom-dashboard-widget-footer)

(setq display-line-numbers-type nil)

(cond ((string-equal (system-name) "localhost")
       (remove-hook '+doom-dashboard-functions #'doom-dashboard-widget-shortmenu)
       (setq display-line-numbers-type 'relative)
       (setq neo-window-width 17)))

(cond ((string-equal (user-real-login-name) "root")
       (menu-bar-mode -1)
       (setq doom-theme 'doom-zenburn)))

;; No more annoying LSP
(setq lsp-ui-sideline-enable nil
      lsp-ui-doc-enable nil
      lsp-enable-symbol-highlighting nil
      +lsp-prompt-to-install-server 'quiet)

;; Package and keymap
(map! (:when (modulep! :ui workspaces)
      :n "C-o" #'+workspace/delete))

(map! (:leader :desc "Neotree Toogle"
      "d" 'neotree-dir-toggle))

(use-package! dired
  :config
  (setq dired-async-mode t)
  (setq dired-omit-files "^\\.?#\\|^\\.[^.].*")
  (define-key dired-mode-map (kbd "C-c ,")   'dired-omit-mode)
  (define-key dired-mode-map (kbd "C-c C-c") 'dired-toggle-read-only))

(use-package! dimmer
  :custom
  (dimmer-adjustment-mode :both)
  (dimmer-fraction 0.5)
  :config
  (dimmer-configure-company-box)
  (dimmer-configure-posframe)
  (dimmer-configure-which-key)
  (add-to-list 'dimmer-exclusion-regexp-list "^\\*Minibuf-[0-9]+\\*")
  (dimmer-mode t))

(use-package! vimrc-mode
  :config
  (add-to-list 'auto-mode-alist '("\\.vim\\(rc\\)?\\'" . vimrc-mode)))

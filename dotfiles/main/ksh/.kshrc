PATH2=$PATH
export PATH=/sbin:/usr/sbin:/usr/local/sbin:$PATH2

PS1='#!@${USER:=$(id -un)}'":\$? \${PWD/#'$HOME'/\~} $ "

export EDITOR=vim
set -o emacs

HISTFILE=~/.ksh_history

export ASDF_DIR="$HOME/.asdf"
. $HOME/.asdf/asdf.sh

if [ -n "$DISPLAY" ]; then
	alias emacs="emacsclient -c -n -a \"\""
else
	alias emacs="emacsclient -a \"\" --nw"
fi

case "$(uname -s)" in
	FreeBSD)
		alias ls="ls -G"
	;;
	Linux)
		[ -d $PREFIX/etc/apt ] && PS1='#!@$?:${PWD/#"$HOME"/\~} $ '

		alias ls="ls --color"
	;;
	OpenBSD)
		. /etc/ksh.kshrc
		PS1='\u@$?:\w [\A]\n \$ '

		command -v colorls >/dev/null && alias ls="colorls -G"
	;;
esac

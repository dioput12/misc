;; -*- no-byte-compile: t; -*-
;;; $DOOMDIR/packages.el

(package! dimmer)
(package! systemd)
(package! vimrc-mode :recipe (:host github :repo "mcandre/vimrc-mode"))

PATH2=$PATH
export PATH=/sbin:/usr/sbin:/usr/local/sbin:$PATH2

if [ "$TERM" = "dumb" ]; then
	return
fi

HISTSIZE=
HISTFILESIZE=
HISTCONTROL=ignoreboth
HISTTIMEFORMAT="[%d/%m/%y %T] "

export EDITOR=vim
export LS_COLORS="${LS_COLORS}:su=30;41:ow=30;42:st=30;44:"
export SYSTEMD_EDITOR=emacs

. ~/.vim/plugged/fzf/shell/completion.bash && . ~/.vim/plugged/fzf/shell/key-bindings.bash
#. $PREFIX/share/fzf/completion.bash && . $PREFIX/share/fzf/key-bindings.bash
[ -f /usr/local/share/bash-completion/bash_completion ] && . /usr/local/share/bash-completion/bash_completion
[ -f /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

. ~/git/gitstatus/gitstatus.prompt.sh
PS1='\[\033[01;32m\]\u\[\033[00m\]@$?:\[\033[01;34m\]\w\[\033[00m\] [\A] \[\033[01;33m\]${GITSTATUS_PROMPT}\n\[\033[00m\] \$ '
#PS1='\[\033[00m\]@$?:\[\033[01;34m\]\w \[\033[01;33m\]${GITSTATUS_PROMPT}\n\[\033[00m\] \$ '
PROMPT_DIRTRIM=0

vterm_printf() {
	if [ -n "$TMUX" ] && { [ "${TERM%%-*}" = "tmux" ] || [ "${TERM%%-*}" = "screen" ] ;}; then
		printf "\ePtmux;\e\e]%s\007\e\\" "$1"
	elif [ "${TERM%%-*}" = "screen" ]; then
		printf "\eP\e]%s\007\e\\" "$1"
	else
		printf "\e]%s\e\\" "$1"
	fi
}

vterm_cmd() {
	local vterm_elisp
	vterm_elisp=""
	while [ $# -gt 0 ]; do
		vterm_elisp="$vterm_elisp""$(printf '"%s" ' "$(printf "%s" "$1" | sed -e 's|\\|\\\\|g' -e 's|"|\\"|g')")"
		shift
	done
	vterm_printf "51;E$vterm_elisp"
}

vterm_prompt_end() {
	if [ -n "$VIMRUNTIME" ]; then
		return
	else
		vterm_printf "51;A$(whoami)@$(hostname):$(pwd)"
	fi
}

PS1=$PS1'\[$(vterm_prompt_end)\]'

[[ "$INSIDE_EMACS" = 'vterm' ]] && alias clear='vterm_printf "51;Evterm-clear-scrollback";tput clear'

. ~/.asdf/asdf.sh
. ~/.asdf/completions/asdf.bash

. ~/.bash_aliases
. ~/.bash_local
